#include <sys/types.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <pcap.h>

#include <unistd.h>

#ifdef __MACH__
    #include <net/ethernet.h>
    #include <netinet/if_ether.h>
#else
    #include <netinet/ether.h>
    #include <netinet/if_ether.h>
    #include <arpa/inet.h>
#endif

#define INET_ADDR_LEN sizeof(in_addr_t)
struct arp_packet {
    struct ether_header eth_header;
    struct ether_arp arp_header;
};

class SendArp
{
private:
    char interface_[IF_NAMESIZE];
    pcap_t *handle_;

    u_int8_t my_mac_[ETHER_ADDR_LEN];
    u_int8_t my_ip_[INET_ADDR_LEN];
    u_int8_t target_mac_[ETHER_ADDR_LEN];

    bool get_my_mac(u_int8_t *mac_addr);
    pcap_t* open_pcap(const char* dev);
    
    bool send_arp(u_char opcode, u_int8_t *sender_mac, u_int8_t *sender_ip, u_int8_t *target_mac, u_int8_t *target_ip);

    bool get_target_mac(u_int8_t *target_ip);
    
    bool resolve_ip_to_mac(u_int8_t *target_ip, u_int8_t *target_mac);
    bool get_my_ip(u_int8_t *ip_addr);

public: 
    SendArp(const char *interface);
    ~SendArp()
    {
        pcap_close(handle_);
    }
    bool attack_arp(const char *victim, const char *gateway);

    u_int8_t* get_my_mac() 
    {
        return my_mac_;
    }
    u_int8_t* get_my_ip()
    {
        return my_ip_;
    }
};